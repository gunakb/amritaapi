module.exports = (req, res) => {
    let {
        query: { name },
      }= req
      let sections=['A','B','C','D','E','F']
      if (!sections.includes(name)){
        name='A'
      }
    res.writeHead(302, {'Location': `https://intranet.cb.amrita.edu/TimeTable/PDF/2020_21/BTech/CSE/BTechCSE${name}4.jpg`});
    
    res.end();
}