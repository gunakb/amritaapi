module.exports = (req, res) => {
	const name = req.query.name || 'visitor!';
	res.status(200).send(`Hello, ${name}!`)
}